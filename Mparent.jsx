import React, { Component } from 'react'

class Parent extends React.Component {
    state = {
        forChild1: 1,
        forChild2: 2,
        // arrayFromChild1: []
    }

    multiMat = () => {
        var res = []
        for (var i = 0; i < 2; i++) {
            var o = []
            for (var j = 0; j < 3; j++) {
                var a = 0
                for (var k = 0; k < 3; k++) {
                    console.log("m1" + j + k, this.state["m1" + j + k], "m2" + i + k, this.state["m2" + i + k])
                                       
                    a = a + this.state["m1" + j + k] * this.state["m2" + i + k]
                    console.log(a, "Latest")
                }
                console.log(a, "Check A")
                o.push(a)
            }
            res.push(o)
        }
        var mainRes = []
        console.log(res, "Check Res")
        for(var i=0; i<3;i++){
            var o=[]
            for(j=0;j<2;j++){
                o.push(res[j][i])
            }
            mainRes.push(o)
        }
        console.log(mainRes, "CHeck main Rws")
        this.setState({mainRes})
    }

    render() {
        console.log(this.state, "CHeck State in Parent")
        
      
        var m1 = []
        
        for (var i = 0; i < 3; i++) {
            var o = []
            for (var j = 0; j < 3; j++) {
                o.push("m1" + i + j)
            }
            m1.push(o)
        }
        var m2 = []

        for (var i = 0; i < 2; i++) {
            var o = []
            for (var j = 0; j < 3; j++) {
                o.push("m2" + i + j)
            }
            m2.push(o)
        }
        var m2Main = []
        for(var i=0; i<3;i++){
            var o=[]
            for(j=0;j<2;j++){
                o.push(m2[j][i])
            }
            m2Main.push(o)
        }
        
        var { forChild1 = -1, forChild2 = 0, child1Value = 9, child2Value = '', arrayFromChild1 = [], mainRes=[] } = this.state
        // var arr = arrayFromChild1.length === undefined ? [] : arrayFromChild1
        console.log(this.state, "Thesre are state variables")
        return <div style={{ textAlign: "center", display: "flex" }}>

            <div>
                {m1.map((d, i) => {
                    return (<div style={{ display: "flex", width: "150px" }}>
                        {d.map((m, n) => {
                            return (<div style={{ width: "50px", height: "50px" }}><input style={{ width: "50px", height: "50px" }} value={this.state[m]}
                                onChange={(e) => this.setState({ [m]:e.target.value })} /></div>)
                        })}
                    </div>)
                })}
            </div>
            <div style={{ width: "50px" }}>*</div>
            <div>
                
                {m2Main.map((d, i) => {
                    return (<div style={{ display: "flex", width: "150px" }}>
                        {d.map((m, n) => {
                            return (<div style={{ width: "50px", height: "50px" }}><input style={{ width: "50px", height: "50px" }} value={this.state[m]}
                                onChange={(e) => this.setState({ [m]: e.target.value })} /></div>)
                        })}
                    </div>)
                })}
            </div>
            <div style={{ width: "50px" }}>=</div>
            <div>
                
                {m2Main.map((d, i) => {
                    return (<div style={{ display: "flex", width: "150px" }}>
                        {d.map((m, n) => {
                            return (<div style={{ width: "50px", height: "50px", border:"1px solid grey" }}>{mainRes[i] ? mainRes[i][n] : ""}</div>)
                        })}
                    </div>)
                })}
            </div>
            <div style={{width:"100px"}}><button onClick={()=> this.multiMat()}>Click for the result</button></div>
        </div>;
    }
}

export default Parent;